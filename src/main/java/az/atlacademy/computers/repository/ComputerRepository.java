package az.atlacademy.computers.repository;

import az.atlacademy.computers.entity.Computer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository

public interface ComputerRepository extends JpaRepository<Computer,Long> {

}
