package az.atlacademy.computers.service;

import az.atlacademy.computers.entity.Computer;
import az.atlacademy.computers.repository.ComputerRepository;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class ComputerService {

    final ComputerRepository computerRepository;

    public List<Computer> getAll(){
        return computerRepository.findAll();
    }

    public Computer getById(Long id){
        return computerRepository.findById(id).get();
    }

    public Computer create(Computer computer){
        return computerRepository.save(computer);
    }

    public void delete(Long id){
        Computer computerForDelete = computerRepository.getById(id);
           computerRepository.delete(computerForDelete);

    }

    public Computer update(Computer computer){
        Computer computerForUpdate = computerRepository.getById(computer.getId());
        computerForUpdate.setId(computer.getId());
        computerForUpdate.setMarka(computer.getMarka());
        computerForUpdate.setModel(computer.getModel());
        computerForUpdate.setCPU(computer.getCPU());
        computerForUpdate.setRAM(computer.getRAM());
        computerForUpdate.setScreen_side(computer.getScreen_side());
        computerForUpdate.setHDD(computer.getHDD());
        computerForUpdate.setDVD(computer.getDVD());
        computerForUpdate.setCamera(computer.getCamera());
        return computerRepository.save(computerForUpdate);
    }

}
