package az.atlacademy.computers.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity
@Table(name= "computers")
public class Computer {

    @Id
    private Long id;
    private String marka;
    private String model;
    private String CPU;
    private String RAM;
    private String screen_side;
    private String HDD;
    private String DVD;
    private String Camera;
}
