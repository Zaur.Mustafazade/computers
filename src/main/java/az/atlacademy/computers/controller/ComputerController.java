package az.atlacademy.computers.controller;

import az.atlacademy.computers.entity.Computer;
import az.atlacademy.computers.service.ComputerService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@AllArgsConstructor
@Slf4j
@RequestMapping("/controllers")
public class ComputerController {
    private ComputerService computerService;

    @GetMapping
    public ResponseEntity<List<Computer>> findAll(){
        return ResponseEntity.ok(computerService.getAll());
    }

    @GetMapping("/id")
    public ResponseEntity<Computer> getById(@PathVariable("id") Long id){
        return ResponseEntity.ok(computerService.getById(id));
    }

    @PostMapping
    public ResponseEntity create(@RequestBody Computer computer){
        return ResponseEntity.ok(computerService.create(computer));
    }
    @DeleteMapping("/{id}")
    public void delete(@PathVariable ("id") Long id){
        computerService.delete(id);
    }

    @PutMapping
    public ResponseEntity update(@RequestBody Computer computer){
        return ResponseEntity.ok(computerService.update(computer));
    }
}
